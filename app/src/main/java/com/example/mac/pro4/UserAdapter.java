package com.example.mac.pro4;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.example.mac.pro4.MODEL.USER;

public class UserAdapter extends ArrayAdapter<USER> {

    private Context context;
    private List<USER> users;

    public UserAdapter(@NonNull Context context, int resource, @NonNull List<USER> objects) {
        super(context, resource, objects);
        this.context = context;
        this.users = objects;
    }

    @Override
    public View getView(final int pos, View converView, ViewGroup parent) {
        LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item, parent , false);

        TextView txtFirstname = (TextView) rowView.findViewById(R.id.txtFirstname);

        // txtFirstname.setText(String.format("%s", users.get(pos).getFirstname()));
        // txtLastname.setText(String.format("%s", users.get(pos).getLastname()));
        txtFirstname.setText(users.get(pos).getTitle());

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Start Activity User form
                Intent intent = new Intent(context, UserActivity.class);
                intent.putExtra("art_id", String.valueOf(users.get(pos).getId()));
                intent.putExtra("art_title", users.get(pos).getTitle());
                intent.putExtra("art_description", users.get(pos).getDescription());
                context.startActivity(intent);
            }
        });
        return rowView;
    }
}
