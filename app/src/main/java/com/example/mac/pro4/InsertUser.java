package com.example.mac.pro4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mac.pro4.MODEL.USER;
import com.example.mac.pro4.REMOTE.APIUtils;
import com.example.mac.pro4.REMOTE.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsertUser extends AppCompatActivity {

    EditText eFirstname,eLastname,eEmail,ePhone;
    Button bSave,bCancel;
    UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_user);

        init();

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                USER user = new USER();
                user.setTitle(eFirstname.getText().toString());
                user.setDescription(eLastname.getText().toString());

                insertUser(user);
                finish();
            }
        });

    }

    private void init()
    {
        eFirstname = (EditText) findViewById(R.id.eFirstname);
        eLastname = (EditText) findViewById(R.id.eLastname);
        bSave = (Button) findViewById(R.id.bSave);
        bCancel = (Button) findViewById(R.id.bCancel);

        userService = APIUtils.getUserServiece();
    }

    public void insertUser(USER user)
    {
        Call<USER> call = userService.addUser(user);
        call.enqueue(new Callback<USER>() {
            @Override
            public void onResponse(Call<USER> call, Response<USER> response) {
                Toast.makeText(InsertUser.this,"Insert User success!!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<USER> call, Throwable t) {
                Log.e("Error",t.getMessage());
            }
        });
    }
}
