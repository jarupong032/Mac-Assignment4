package com.example.mac.pro4.REMOTE;

import com.example.mac.pro4.MODEL.USER;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface UserService {

    @GET("articles/")
    Call<List<USER>> getUser();

    @POST("articles/")
    Call<USER> addUser(@Body USER user);

    @PUT("articles/{id}")
    Call<USER> updateUser(@Path("id") int id, @Body USER user);

    @DELETE("articles/{id}")
    Call<USER> deleteUser(@Path("id") int id);



}
