package com.example.mac.pro4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mac.pro4.MODEL.USER;
import com.example.mac.pro4.REMOTE.APIUtils;
import com.example.mac.pro4.REMOTE.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserActivity extends AppCompatActivity {

    EditText edtFirstname,edtLastname;
    Button btnSave,btnCancel,btnDelete;
    UserService userService;
    String userId,userFirstname,userLastname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        init();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                USER user = new USER();
                user.setTitle(edtFirstname.getText().toString());
                user.setDescription(edtLastname.getText().toString());

                updateUser(Integer.parseInt(userId),user);
                finish();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteUser(Integer.parseInt(userId));
                finish();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    //Method for Add new User
    /*
    public void addUser(User user)
    {
        Call<User> call = userService.addUser(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful())
                {
                    Toast.makeText(UserActivity.this,"User Create success!! ",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                    Log.e("Error",t.getMessage());
            }
        });
    }
    */

    public void updateUser(int id, USER user)
    {
        Call<USER> call = userService.updateUser(id, user);
        call.enqueue(new Callback<USER>() {
            @Override
            public void onResponse(Call<USER> call, Response<USER> response) {
                if (response.isSuccessful())
                {
                    Toast.makeText(UserActivity.this,"User Update success!! ",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<USER> call, Throwable t) {
                Log.e("Error",t.getMessage());
            }
        });
    }



    public void deleteUser(int id)
    {
        Call<USER> call = userService.deleteUser(id);
        call.enqueue(new Callback<USER>() {
            @Override
            public void onResponse(Call<USER> call, Response<USER> response) {
                if (response.isSuccessful())
                {
                    Toast.makeText(UserActivity.this,"User Delete success!! ",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<USER> call, Throwable t) {
                Log.e("Error",t.getMessage());
            }
        });
    }
    private void init() {
        edtFirstname = (EditText) findViewById(R.id.edtFirstname);
        edtLastname = (EditText) findViewById(R.id.edtLastname);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnDelete = (Button) findViewById(R.id.btnDelete);

        userService = APIUtils.getUserServiece();

        Bundle extras = getIntent().getExtras();
        userId = extras.getString("art_id");
        userFirstname = extras.getString("art_title");
        userLastname = extras.getString("art_description");


        edtFirstname.setText(userFirstname);
        edtLastname.setText(userLastname);
    }
}
