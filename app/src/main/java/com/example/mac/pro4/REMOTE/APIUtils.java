package com.example.mac.pro4.REMOTE;

public class APIUtils {

    private APIUtils(){

    };

    public static final String API_URL = "https://xsintern-api.learnbalance.com/api/";

    public static UserService getUserServiece(){
        return RetrofitClient.getClient(API_URL).create(UserService.class);
    }
}

